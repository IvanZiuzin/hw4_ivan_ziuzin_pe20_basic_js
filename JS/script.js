"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------
// Реализовать функцию, которая будет выполнять математические операции с числами, которые ввёл пользователь
// Задача на чистом Javascript без использования библиотек jQuery или React.

// Технические требования: 

// Получить через модальное окно браузера два числа 
// Получить при помощи модального окна браузера математическую операцию, которую необходимо выполнить 
// Сюда может быть введены следующие операторы: +, -, *, / 
// Создать функцию, в которую передать два значения пользователя и операцию
// Вывести в консоль результат выполнения функции

// Необязательные задания повышенной сложности 
// После ввода данных добавить проверку их корректности 
// Если пользователь не ввёл числа, или при вводе указал не числа - cпросить оба числа снова 
// При этом значением по умолчанию для кажной пременной должна быть информация введённая заранее


// TASK #1 ------- < function > ----------

// Декомпозиция задачи: разделим задачу на простые операции: 

// - Это Функция №1:--------------------
// 1/ - Запрашивает ввод данных пользователем и возвращает введённое значение
// 2/ - Проверяет корректность введённых данных: это число, это не пробел, это не пустая строка

// - Это Функция №2:--------------------
// 3/ - Запрашивает ввод пользователем математического оператора и возвращает введённое значение
// 4/ - Проверяет: если второе число 0 и оператор / - просит заново ввести оператор (на 0 делить нельзя)
// 5/ - Проверяет: ввел ли пользователь именно нужные операторы:  +, -, *, / . Если нет - запрашивает снова ввести оператор

// - Это Функция №3:--------------------
// 6/ - Совершает метематический расчёт с указанным оператором и числами, возвращает результат 

// - Это Функция №4:--------------------
// 7/ - Выводит результат расчета в консоль и в модальное окно для пользователя

// СТАРТ ЗАДАЧИ -------------------------------------------------------------------

console.log ('Task #1 < function > started  \u{1F680}')

alert(`Hi!\nTo start  - enter a pair of numbers.\nNotice:\nThe default number is 5.\nBut You can replace default value with any other number.\nLet's start! \u{1F6B4}`);

let firstNum = getNumber(); // Переменная 1 - Первое число пользователя 
alert(`Perfect!  \u{1F63B} Your first number is ${firstNum}`);

let secondNum = getNumber(); // Переменная 2 - Второе число польвателя 
alert(`Nice! \u{1F63C} Your second number is ${secondNum}`);

alert(`Now, enter one of the following mathematical operator:\n"\u{2795}" - operator will add the number ${firstNum} and ${secondNum};\n"\u{2796}" - operator will subtract from number ${firstNum} number ${secondNum};\n" \u{2731} " - operator will multiply number ${firstNum} and number ${secondNum};\n" \u{002F} " - operator will divide number ${firstNum} into number ${secondNum};\nYour chosen operator will be used for calculation \u{2198}`);
let mathOperator = getMathOperator(); // Переменная 3 - Опреатор пользователя
alert(`Let's calс the result \u{1F9D0}: ${firstNum} ${mathOperator} ${secondNum}:`);

let calcResult = calcUserNumbers(firstNum, secondNum, mathOperator);// Переменная 4 - Результат вычисления 

console.log(typeof firstNum, firstNum, 'This is your first number' );
console.log(typeof secondNum, secondNum,'This is your second number');
console.log(typeof mathOperator, mathOperator, 'This is your math operator');
showCalcResult()

console.log ('Task #1 < function > finished!:) \u{1F60E} \u{1F3C1}')

// ФУНКЦИИ ---------------------------------------------------------------------------

// Функция №1:-------------------------------------------------------------------------
// Получение и проверка чисел от пользователя
function getNumber() {
    numCheck:while (true) {
        let num = parseFloat(prompt('Please enter number.\nIt may be any number you want.\nUse only figures\n(You can replace default number with your own one)', 5))
        if (!Number.isNaN(num)) {
            return num;  
        } else{
            alert(`You entered ${num} - it's not a number!\u{26D4}\nTry to enter another numbers!`);
            continue numCheck;
        }
    }
}

// Функция №2:-------------------------------------------------------------------------
// Получение и проверка математических операторов от пользователя
function getMathOperator() {
    operCheck:while (true) {
        let mathUserOper = prompt('\n "\u{2795}"\n "\u{2796}"\n " \u{2731} "\n " \u{002F} "\nthis operator will be used for calculation', '+');
        if (mathUserOper === "/" && secondNum === 0) {
            alert(`Sorry! \u{26A0} Impossible \u{1F6AB}: ${firstNum} ${mathUserOper} ${secondNum }\n- you cannot divide by zero!\nTry to enter another operator!`);
            continue operCheck;
        } else if(mathUserOper === "+" || mathUserOper === "-" || mathUserOper === "*" || mathUserOper === "/"){
            return mathUserOper;
        } else{
            alert(`You entered invalid operator ${mathUserOper}\nTry to enter another operator!`);
            continue operCheck;
        }
    }  
}

// Функция №3:-------------------------------------------------------------------------
// Вычисление результата с числами и математическим оператором от пользователя
function calcUserNumbers(a, b, c) {
    let mathOperator = c;
    switch (mathOperator) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return a / b;
        default:
            alert('Error, no data');
            console.log('Error, no data');
            break;
    }
        
}

// Функция №4: -------------------------------------------------------------------------
// Показ результата расчетов 
function showCalcResult() {
    alert(`This is your calculation result: ${calcResult} \u{1F60E}`);
    console.log(typeof calcResult, calcResult, 'This is your calculation result \u{1F3AF}');
}

// ФИНИШ ЗАДАЧИ -------------------------------------------------------------------------





// ЗАМЕТКИ ДЛЯ МЕНЯ
// -------------------------------------------------------------------------------------

// По методу Number.isNaN();
// Number.isNaN(NaN);        // true
// Number.isNaN(123);        // false
// Number.isNaN("строка");   // false
// Number.isNaN(undefined);  // false
// Number.isNaN(null);       // false


//____________________________________________

// По методу isNaN();
// isNaN(NaN);       // true
// isNaN(undefined); // true
// isNaN({});        // true

// isNaN(true);      // false
// isNaN(null);      // false
// isNaN(37);        // false

// strings
// isNaN("37");      // false: "37" преобразуется в число 37 которое не NaN
// isNaN("37.37");   // false: "37.37" преобразуется в число 37.37 которое не NaN
// isNaN("");        // false: пустая строка преобразуется в 0 которое не NaN
// isNaN(" ");       // false: строка с пробелом преобразуется в 0 которое не NaN
// isNaN("37,5");    // true

// Даты
// isNaN(new Date());                // false
// isNaN(new Date().toString());     // true

// Пример почему использование isNaN не всегда уместно
// isNaN("blabla")   // true: "blabla" преобразовано в число.
// При парсинге преобразуется в число при неудаче возвращает NaN

// Важное различие между ними заключается в том, что:

// || возвращает первое истинное значение.
// "??" возвращает первое определённое значение.
// Проще говоря, оператор || не различает false, 0, 
// пустую строку "" и null/undefined. Для него они все одинаковы, т.е. 
// являются ложными значениями. Если первым аргументом для оператора || будет 
// любое из перечисленных значений, то в качестве результата мы получим второй аргумент.

